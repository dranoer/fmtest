package com.dranoer.fmtest.util

enum class Status {
    LOADING, ERROR, DONE
}
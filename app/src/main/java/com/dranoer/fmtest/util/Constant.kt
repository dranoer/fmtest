package com.dranoer.fmtest.util

interface Constant {

    companion object {
        const val BASE_URL = "https://api.farhadmarket.com/apiv2/"
    }

}
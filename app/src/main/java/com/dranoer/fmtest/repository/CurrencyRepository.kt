package com.dranoer.fmtest.repository

import com.dranoer.fmtest.api.ApiService
import com.dranoer.fmtest.api.SafeApiRequest
import com.dranoer.fmtest.model.Item

class CurrencyRepository(private val api: ApiService) : SafeApiRequest() {

    suspend fun fetchPrices(): List<Item> {
        return apiRequest { api.getPrices() }
    }
}
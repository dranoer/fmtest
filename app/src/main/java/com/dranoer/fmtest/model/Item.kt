package com.dranoer.fmtest.model

import com.google.gson.annotations.SerializedName

data class Item(

    @field:SerializedName("id")
    val id: Long,

    @field:SerializedName("ticker")
    val ticker: String,

    @field:SerializedName("swing")
    val swing: String,

    @field:SerializedName("currencySymbol")
    val currencySymbol: String,

    @field:SerializedName("criterionCurrencySymbol")
    val criterionCurrencySymbol: String,

    @field:SerializedName("currency")
    val currency: Currency,

    @field:SerializedName("criterionCurrency")
    val criterionCurrency: Currency
)
package com.dranoer.fmtest.model

import com.google.gson.annotations.SerializedName

data class Currency(

    @field:SerializedName("symbol")
    val symbol: String,

    @field:SerializedName("Tether")
    val Tether: String,

    @field:SerializedName("swapMin")
    val swapMin: String,

    @field:SerializedName("swapMax")
    val swapMax: String,

    @field:SerializedName("swapStep")
    val swapStep: String
)
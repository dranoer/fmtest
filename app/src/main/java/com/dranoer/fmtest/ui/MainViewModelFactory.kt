package com.dranoer.fmtest.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dranoer.fmtest.repository.CurrencyRepository

class MainViewModelFactory(var currencyRepository: CurrencyRepository) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(currencyRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel Class")
    }
}
package com.dranoer.fmtest.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dranoer.fmtest.model.Item
import com.dranoer.fmtest.repository.CurrencyRepository
import com.dranoer.fmtest.util.Status
import kotlinx.coroutines.launch

class MainViewModel(var currencyRepository: CurrencyRepository) : ViewModel() {

    private var currencyList: List<Item>? = null

    private var _currencies = MutableLiveData<List<Item>>()
    val currencies: LiveData<List<Item>>?
        get() = _currencies

    private val _status = MutableLiveData<Status>()
    val status: LiveData<Status>
        get() = _status

    // Get list of prices
    fun getCurrencies() {
        viewModelScope.launch {
            try {
                currencyList = currencyRepository.fetchPrices()
                _status.value = Status.LOADING

                _currencies.value = currencyList
                _status.value = Status.DONE

            } catch (e: Exception) {
                _status.value = Status.ERROR
                Log.d("nazi", "Error >> ${_status?.value} >> ${e.message} ")
                e.printStackTrace()
            }
        }
    }
}
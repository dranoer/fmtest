package com.dranoer.fmtest.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.dranoer.fmtest.R
import com.dranoer.fmtest.api.ApiService
import com.dranoer.fmtest.api.RequestInterceptor
import com.dranoer.fmtest.databinding.FragmentMainBinding
import com.dranoer.fmtest.repository.CurrencyRepository

class MainFragment : Fragment(),
    AdapterView.OnItemSelectedListener {

    lateinit var binding: FragmentMainBinding
    lateinit var mainViewModel: MainViewModel

    lateinit var currencySpinner: Spinner
    lateinit var criterionCurrencySpinner: Spinner

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_main, container, false
        )

        val viewModelFactory =
            MainViewModelFactory(CurrencyRepository(ApiService.invoke(RequestInterceptor())))
        mainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        binding.mainViewModel = mainViewModel
        binding.lifecycleOwner = this

        mainViewModel.getCurrencies()

        binding.submit.setOnClickListener {
            mainViewModel.getCurrencies()
        }

        initSpinner()

        return binding.root
    }

    fun initSpinner() {

        currencySpinner = binding.tvPlusPay

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.planets_array,
            R.layout.spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_item)
            // Apply the adapter to the spinner
            currencySpinner.adapter = adapter
//            spinner.setSelection(0)
        }

        currencySpinner.onItemSelectedListener = this
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        Log.d("nzn", "p2 > $p2, p3 > $p3 ,, pa > $p1 ,, p0 > $p0")
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

}
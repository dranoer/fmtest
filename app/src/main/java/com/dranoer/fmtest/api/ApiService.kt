package com.dranoer.fmtest.api

import com.dranoer.fmtest.model.Item
import com.dranoer.fmtest.util.Constant
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiService {

    companion object {
        operator fun invoke(
            interceptor: RequestInterceptor
        ): ApiService {

            val gson = GsonBuilder().setLenient().create()

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(ApiService::class.java)
        }
    }

    @GET("prices")
    suspend fun getPrices(): Response<List<Item>>
}